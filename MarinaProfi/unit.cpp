#include <iostream>

#include "unit.h"


Unit::Unit() {
    score_ = 0;
    lives_ = 3;
}


void Rook::Info() const {
    std::cout << "You can move your piece using these commands:\n";
    std::cout << "Up <x> - move up to <x> cells:\n";
    std::cout << "Right <x> - move right to <x> cells:\n";
    std::cout << "Down <x> - move down to <x> cells:\n";
    std::cout << "Left <x> - move left to <x> cells:\n";
    std::cout << "\n";
}


void Bishop::Info() const {
    std::cout << "You can move your piece using these commands:\n";
    std::cout << "UpRight <x> - move up-right to <x> cells:\n";
    std::cout << "DownRight <x> - move down-right to <x> cells:\n";
    std::cout << "UpLeft <x> - move up-left to <x> cells:\n";
    std::cout << "DownLeft <x> - move down-left to <x> cells:\n";
    std::cout << "\n";
}


std::string Rook::GetName() {
    return "Rook";
}


std::string Bishop::GetName() {
    return "Bishop";
}