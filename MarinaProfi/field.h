#ifndef FIELD_H
#define FIELD_H


#include <vector>

#include "unit.h"

class Field {
public:
    Field(int n=8, int m=8);

    const int n_, m_;
    std::vector<std::vector<char>> field_;
    Unit *unit_ = nullptr;

    void PlaceUnit(Unit* unit, int i=0, int j=0, bool gen=false);
    void Print() const;

protected:
    void Info() const;
    void Generate();
};


#endif //FIELD_H
