#include <iostream>

#include "field.h"

int main() {
    Field field;
    std::cout << "Auto-generated field:\n";
    field.Print();

    std::string piece_name;
    std::cout << "Enter the name of piece you want to place (Rook/Bishop): ";
    std::cin >> piece_name;
    while (piece_name != "Rook" && piece_name != "Bishop") {
        std::cout << "Invalid piece name\n";
        std::cout << "Enter the name of piece you want to place (Rook/Bishop): ";
        std::cin >> piece_name;
    }

    int piece_i, piece_j;
    do {
        std::cout << "Enter the coordinates of your piece (x, y): ";
        std::cin >> piece_i >> piece_j;
        if (piece_name == "Rook") {
            field.PlaceUnit(new Rook, piece_i, piece_j);
        } else if (piece_name == "Bishop") {
            field.PlaceUnit(new Bishop, piece_i, piece_j);
        }
    } while (field.unit_ == nullptr);

    std::cout << "The field:\n";
    field.Print();

    field.unit_ -> Info();
    std::cout << "When your lives run out, program will shutdown\n\n";

    while (field.unit_ -> lives_) {
        std::cout << "Enter you turn: ";
        std::string type;
        int len;
        std::cin >> type >> len;
        field.unit_->Turn(type, len);
        std::cout << "The field:\n";
        field.Print();
    }
    std::cout << "--shutting down--";
    return 0;
}
