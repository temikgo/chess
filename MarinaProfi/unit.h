#ifndef UNIT_H
#define UNIT_H

#include <string>

class Field;

class Unit {
public:
    Unit();

    int i_, j_;
    int score_;
    int lives_;
    Field* field_ = nullptr;

    void DoTurn(std::pair<int, int> turn, const int len);
    virtual void Info() const = 0;
    virtual std::string GetName() = 0;
    virtual void Turn(const std::string& type, const int len) = 0;
};

class Rook: public Unit {
    void Info() const override;
    std::string GetName() override;
    void Turn(const std::string& type, const int len) override;
};

class Bishop: public Unit {
    void Info() const override;
    std::string GetName() override;
    void Turn(const std::string& type, const int len) override;
};


#endif //UNIT_H
