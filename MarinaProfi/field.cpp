#include <iostream>
#include <chrono>
#include <random>

#include "field.h"

Field::Field(int n, int m)
    : n_(n), m_(m) {
    field_.resize(n, std::vector<char>(m));
    Info();
    Generate();
}


void Field::Info() const {
    std::cout << "Field legend:\n";
    std::cout << "_ - empty cell\n";
    std::cout << "0 - obstacle\n";
    std::cout << "$ - cell with a point\n";
    std::cout << "* - dangerous cell\n";
    std::cout << "R - Rook piece\n";
    std::cout << "B - Bishop piece\n";
    std::cout << "\n";
}


void Field::Generate() {
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    for (size_t i = 0; i < n_; ++i) {
        for (size_t j = 0; j < m_; ++j) {
            int x = gen() % 4;
            if (x == 0) {
                field_[i][j] = '_';
            } else if (x == 1) {
                field_[i][j] = '0';
            } else if (x == 2) {
                field_[i][j] = '$';
            } else if (x == 3) {
                field_[i][j] = '*';
            }
        }
    }
}


void Field::PlaceUnit(Unit *unit, int i, int j, bool gen) {
    if (gen) {
        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        std::mt19937 gen(seed);
        i = gen() % n_;
        j = gen() % m_;
    } else if (i < 0 || i >= n_ || j < 0 || j >= m_) {
        std::cout << "Coordinates are out of field\n\n";
        return;
    }
    if (field_[i][j] != '_') {
        std::cout << "You can add unit only to the empty cell\n\n";
        return;
    }
    unit -> i_ = i;
    unit -> j_ = j;
    unit -> field_ = this;
    unit_ = unit;
    field_[i][j] = (unit_ -> GetName())[0];
    std::cout << unit_ -> GetName() << " was successfully added to the field\n\n";
}


void Field::Print() const {
    for (size_t i = 0; i < n_; ++i) {
        for (size_t j = 0; j < m_; ++j) {
            std::cout << field_[i][j];
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}


void Unit::DoTurn(std::pair<int, int> turn, const int len) {
    int cur_i = i_, cur_j = j_;
    for (size_t i = 0; i < len; ++i) {
        cur_i += turn.first;
        cur_j += turn.second;
        if (cur_i < 0 || cur_i >= (field_ -> n_) || cur_j < 0 || cur_j >= (field_ -> m_)) {
            std::cout << "Incorrect turn (out of the field)\n\n";
            return;
        }
        if ((field_ -> field_)[cur_i][cur_j] == '0') {
            std::cout << "Incorrect turn (obstacles on the way)\n\n";
            return;
        }
    }
    (field_ -> field_)[i_][j_] = '_';
    i_ = cur_i; j_ = cur_j;
    if ((field_ -> field_)[i_][j_] == '$') {
        std::cout << "$ collected! You score: " << ++score_ << "\n\n";
    } else if ((field_ -> field_)[i_][j_] == '*') {
        --lives_;
        if (lives_) {
            std::cout << "Oops, you lost life! You lives: " << lives_ << "\n\n";
        } else {
            std::cout << "Oops, you lost life! It was the last one\n\n";
        }
    }
    (field_ -> field_)[i_][j_] = GetName()[0];
}


void Rook::Turn(const std::string& type, const int len) {
    const std::pair<int, int> turns[4] =
            {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    std::pair<int, int> turn;
    if (type == "Up") {
        turn = turns[0];
    } else if (type == "Right") {
        turn = turns[1];
    } else if (type == "Down") {
        turn = turns[2];
    } else if (type == "Left") {
        turn = turns[3];
    } else {
        std::cout << "Incorrect direction\n";
        return;
    }
    DoTurn(turn, len);
}


void Bishop::Turn(const std::string& type, const int len) {
    const std::pair<int, int> turns[4] =
            {{-1, 1}, {1, 1}, {1, -1}, {-1, -1}};
    std::pair<int, int> turn;
    if (type == "UpRight") {
        turn = turns[0];
    } else if (type == "DownRight") {
        turn = turns[1];
    } else if (type == "DownLeft") {
        turn = turns[2];
    } else if (type == "UpLeft") {
        turn = turns[3];
    } else {
        std::cout << "Incorrect direction\n";
        return;
    }
    DoTurn(turn, len);
}